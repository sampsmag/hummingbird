from rest_framework import generics

from hummingbird.rooms.models import MeetingRoom
from hummingbird.rooms.serializers import MeetingRoomSerializer

__all__ = [
    'MeetingRoomsView',
    'MeetingRoomDetailView'
]


class MeetingRoomsView(generics.ListCreateAPIView):
    queryset = MeetingRoom.objects.order_by('name')
    serializer_class = MeetingRoomSerializer


class MeetingRoomDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = MeetingRoom.objects.all()
    serializer_class = MeetingRoomSerializer
