from django.db import models


class MeetingRoom(models.Model):
    """
    Model for a meeting room. Users will schedule meetings in these
    rooms, see meetings.Appointment to know more.
    """
    name = models.CharField(max_length=64, unique=True)
