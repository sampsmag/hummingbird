import logging
from rest_framework import serializers

from hummingbird.rooms.models import MeetingRoom

_logger = logging.getLogger(__name__)


class MeetingRoomSerializer(serializers.ModelSerializer):
    class Meta:
        model = MeetingRoom
        fields = '__all__'

    def create(self, *args, **kwargs):
        room = super().create(*args, **kwargs)
        _logger.info('Room %s with name %s created', room.id, room.name)

        return room

    def update(self, *args, **kwargs):
        room = super().update(*args, **kwargs)
        _logger.info('Room %s with name %s updated', room.id, room.name)

        return room
