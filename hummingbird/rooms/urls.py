from django.urls import path

from hummingbird.rooms.views import MeetingRoomsView, MeetingRoomDetailView


app_name = 'rooms'
urlpatterns = [
    path('', MeetingRoomsView.as_view(), name='index'),
    path('<int:pk>', MeetingRoomDetailView.as_view(), name='detail'),
]
