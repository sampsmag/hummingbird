import pytest

import json
import logging
from django.urls import reverse
from rest_framework import status

from hummingbird.rooms.models import MeetingRoom

pytestmark = pytest.mark.django_db


def test_create_new_room(caplog, client):
    room_name = 'Room 1'
    response = client.post(reverse('rooms:index'), data={
        'name': room_name,
    })

    assert response.status_code == status.HTTP_201_CREATED
    content = json.loads(response.content)
    assert content.get('name') == room_name

    log_message = caplog.record_tuples[0]
    assert log_message == ('hummingbird.rooms.serializers', logging.INFO,
                           f"Room {content['id']} with name {content['name']} created")


def test_create_room_empty_name_fails(client):
    response = client.post(reverse('rooms:index'))
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert json.loads(response.content) == {
        'name': ['This field is required.']
    }

    response = client.post(reverse('rooms:index'), data={'name': ''})
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert json.loads(response.content) == {
        'name': ['This field may not be blank.']
    }


def test_create_new_room_duplicated_name_fails(client):
    room_name = 'Room 1'
    MeetingRoom.objects.create(name=room_name)

    response = client.post(reverse('rooms:index'), data={
        'name': room_name,
    })

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert json.loads(response.content) == {
        'name': ['meeting room with this name already exists.'],
    }


def test_get_list_of_rooms_ordered_by_name(client):
    MeetingRoom.objects.bulk_create([
        MeetingRoom(id=1, name='Theta'),
        MeetingRoom(id=2, name='Omega'),
        MeetingRoom(id=3, name='Lambda'),
        MeetingRoom(id=4, name='Alpha'),
        MeetingRoom(id=5, name='Beta'),
        MeetingRoom(id=6, name='Gamma'),
    ])

    response = client.get(reverse('rooms:index'))

    assert response.status_code == status.HTTP_200_OK
    assert json.loads(response.content) == [
        {'id': 4, 'name': 'Alpha'},
        {'id': 5, 'name': 'Beta'},
        {'id': 6, 'name': 'Gamma'},
        {'id': 3, 'name': 'Lambda'},
        {'id': 2, 'name': 'Omega'},
        {'id': 1, 'name': 'Theta'},
    ]


def test_retrieve_room_details(client):
    room = MeetingRoom.objects.create(name='my room!')

    response = client.get(reverse('rooms:detail', args=[room.id]))

    assert response.status_code == status.HTTP_200_OK
    assert json.loads(response.content) == {
        'id': room.id,
        'name': room.name
    }


def test_delete_room(client):
    room_name = 'my room!'
    room = MeetingRoom.objects.create(name=room_name)
    deleted_room_id = room.id

    response = client.delete(reverse('rooms:detail', args=[room.id]))

    assert response.status_code == status.HTTP_204_NO_CONTENT

    # now, to ensure we're able to re-create a room with the name name
    # _but_ with a different id
    response = client.post(reverse('rooms:index'), data={'name': room_name})
    assert response.status_code == status.HTTP_201_CREATED

    assert MeetingRoom.objects.get(name=room_name).id != deleted_room_id


def test_delete_inexistent_room(client):
    response = client.delete(reverse('rooms:detail', args=[0]))

    assert response.status_code == status.HTTP_404_NOT_FOUND


def test_update_room_name(caplog, client):
    room = MeetingRoom.objects.create(name='Room 1.0')

    response = client.put(reverse('rooms:detail', args=[room.id]), data={
        'name': 'Room 1.1'
    }, content_type='application/json')

    assert response.status_code == status.HTTP_200_OK
    room.refresh_from_db()
    assert room.name == 'Room 1.1'
    log_message = caplog.record_tuples[0]
    assert log_message == ('hummingbird.rooms.serializers', logging.INFO,
                           f"Room {room.id} with name {room.name} updated")

    # Works with patch too!
    response = client.patch(reverse('rooms:detail', args=[room.id]), data={
        'name': 'Room 1.2'
    }, content_type='application/json')

    assert response.status_code == status.HTTP_200_OK
    room.refresh_from_db()
    assert room.name == 'Room 1.2'
    log_message = caplog.record_tuples[1]
    assert log_message == ('hummingbird.rooms.serializers', logging.INFO,
                           f"Room {room.id} with name {room.name} updated")


def test_update_room_name_with_conflict(client):
    target_room_id = 1
    MeetingRoom.objects.bulk_create([
        MeetingRoom(id=target_room_id, name='Room A'),
        MeetingRoom(id=2, name='Room B'),
    ])

    response = client.put(reverse('rooms:detail', args=[target_room_id]), data={
        'name': 'Room B'
    }, content_type='application/json')

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert json.loads(response.content) == {
        'name': ['meeting room with this name already exists.']
    }


def test_put_room_empty_name_fails(client):
    room_id = MeetingRoom.objects.create(name='Room 1').id

    response = client.put(reverse('rooms:detail', args=[room_id]), data={
        'name': ''
    }, content_type='application/json')

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert json.loads(response.content) == {
        'name': ['This field may not be blank.']
    }

    response = client.put(reverse('rooms:detail', args=[room_id]), content_type='application/json')

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert json.loads(response.content) == {
        'name': ['This field is required.']
    }


def test_patch_room_empty_name_fails(client):
    room_id = MeetingRoom.objects.create(name='Room 1').id

    response = client.patch(reverse('rooms:detail', args=[room_id]), data={
        'name': ''
    }, content_type='application/json')

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert json.loads(response.content) == {
        'name': ['This field may not be blank.']
    }

    response = client.patch(reverse('rooms:detail', args=[room_id]), content_type='application/json')
    assert response.status_code == status.HTTP_200_OK
    assert MeetingRoom.objects.get(id=room_id).name == 'Room 1'
