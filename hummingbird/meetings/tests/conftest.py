import pytest
import random
from model_mommy import mommy

from hummingbird.meetings.models import Appointment
from hummingbird.rooms.models import MeetingRoom
from .test_utils import tzdatetime


@pytest.fixture
def room():
    return mommy.make(MeetingRoom)


@pytest.fixture
def appointment(period):
    return mommy.make(Appointment, period=period)


@pytest.fixture
def period():
    """
    Returns a random (start, end) tuple of timestamps.
    The pair is timezone-aware. end is always calculated
    to be 1 hour after start.
    """
    year = random.randint(2000, 2020)
    month = random.randint(1, 12)
    day = random.randint(1, 28)
    hour = random.randint(7, 17)
    minute = random.randint(1, 30)

    start = tzdatetime(year, month, day, hour, minute)
    end = tzdatetime(year, month, day, hour + 1, minute)

    return (start, end)
