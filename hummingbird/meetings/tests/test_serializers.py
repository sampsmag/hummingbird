import pytest
from model_mommy import mommy
from unittest.mock import patch

import logging
from datetime import timedelta

from django.db.utils import IntegrityError
from rest_framework import serializers

from hummingbird.meetings.serializers import AppointmentSerializer
from hummingbird.rooms.models import MeetingRoom
from .test_utils import tzdatetime

pytestmark = pytest.mark.django_db


def test_appointment_serializer_contains_expected_fields(appointment):
    serializer = AppointmentSerializer(appointment)

    assert serializer.data == {
        'id': appointment.id,
        'room': appointment.room_id,
        'title': appointment.title,
        'start': appointment.starts_at,
        'end': appointment.ends_at
    }


def test_appointment_serializer_creates_new_instance(caplog, room, period):
    serializer = AppointmentSerializer(data={
        'title': 'My appointment',
        'room': room.id,
        'start': period[0],
        'end': period[1]
    })

    assert serializer.is_valid(), serializer.errors

    appointment = serializer.save()
    assert appointment.title == 'My appointment'
    assert appointment.room_id == room.id
    assert appointment.starts_at == period[0]
    assert appointment.ends_at == period[1]

    log_message = caplog.record_tuples[0]
    assert log_message == ('hummingbird.meetings.serializers', logging.INFO,
                           f'Appointment {appointment.id} for room {room.id} created at [{period[0]}, {period[1]})')


@pytest.mark.parametrize('start, end', [
    (tzdatetime(2011, 2, 1), tzdatetime(2010, 12, 25)),
    (tzdatetime(2011, 2, 1), tzdatetime(2011, 2, 1)),
])
def test_appointment_serializer_create_with_wrong_period_fails(room, start, end):
    serializer = AppointmentSerializer(data={
        'title': 'My appointment',
        'room': room.id,
        'start': start,
        'end': end
    })

    assert serializer.is_valid() is False
    assert str(serializer.errors['period'][0]) == '"end" value must be greater than "start" value.'


@patch('rest_framework.serializers.ModelSerializer.create')
def test_appointment_serializer_create_unknown_integrity_error_reraises(super_create_mock, caplog, room, period):
    super_create_mock.side_effect = IntegrityError('This is an unknown error!')
    serializer = AppointmentSerializer(data={
        'title': 'My appointment',
        'room': room.id,
        'start': period[0],
        'end': period[1]
    })

    assert serializer.is_valid()

    with pytest.raises(IntegrityError) as excinfo:
        serializer.save()

    assert str(excinfo.value) == 'This is an unknown error!'


def test_appointment_serializer_with_period_overlap_fails(appointment):
    serializer = AppointmentSerializer(data={
        'room': appointment.room_id,
        'title': 'Celeste',
        'start': appointment.starts_at,
        'end': appointment.ends_at
    })

    assert serializer.is_valid()
    with pytest.raises(serializers.ValidationError) as excinfo:
        serializer.save()

    errors = excinfo.value.detail
    assert (str(errors['period'][0])
            == 'Given "start" and "end" values overlap with another appointment in the same room.')


def test_appointment_serializer_updates_instances_period(caplog, appointment):
    new_start = appointment.starts_at + timedelta(hours=1)
    new_end = appointment.ends_at + timedelta(hours=1)

    serializer = AppointmentSerializer(appointment, data={
        'start': new_start,
        'end': new_end
    }, partial=True)

    assert serializer.is_valid(), serializer.errors
    serializer.save()

    appointment.refresh_from_db()
    assert appointment.starts_at == new_start
    assert appointment.ends_at == new_end

    log_message = caplog.record_tuples[0]
    assert log_message == ('hummingbird.meetings.serializers', logging.INFO,
                           (f'Appointment {appointment.id} updated to room {appointment.room_id} at '
                            f'[{appointment.starts_at}, {appointment.ends_at})'))


def test_appointment_serializer_updates_instances_room(appointment):
    new_room = mommy.make(MeetingRoom)

    serializer = AppointmentSerializer(appointment, data={
        'room': new_room.id,
    }, partial=True)

    assert serializer.is_valid(), serializer.errors
    serializer.save()

    appointment.refresh_from_db()
    assert appointment.room_id == new_room.id


def test_appointment_serializer_updates_instances_starts_at(appointment):
    new_starts_at = appointment.ends_at - timedelta(days=1)
    original_ends_at = appointment.ends_at

    serializer = AppointmentSerializer(appointment, data={
        'start': new_starts_at,
    }, partial=True)

    assert serializer.is_valid(), serializer.errors
    serializer.save()

    appointment.refresh_from_db()
    assert appointment.starts_at == new_starts_at
    assert appointment.ends_at == original_ends_at


def test_appointment_serializer_updates_instances_ends_at(appointment):
    new_ends_at = appointment.starts_at + timedelta(days=1)
    original_starts_at = appointment.starts_at

    serializer = AppointmentSerializer(appointment, data={
        'end': new_ends_at,
    }, partial=True)

    assert serializer.is_valid(), serializer.errors
    serializer.save()

    appointment.refresh_from_db()
    assert appointment.ends_at == new_ends_at
    assert appointment.starts_at == original_starts_at


def test_appointment_serializer_partial_updates_instances_with_invalid_starts_at(appointment):
    # If we only send `start` or `end` in a way that breaks the `end > start` rule,
    # the serializer needs to fail in a safe way
    values = [
        appointment.ends_at,
        appointment.ends_at + timedelta(days=1)
    ]
    for wrong_starts_at in values:
        serializer = AppointmentSerializer(appointment, data={
            'start': wrong_starts_at,
        }, partial=True)

        assert not serializer.is_valid()
        assert str(serializer.errors['period'][0]) == '"end" value must be greater than "start" value.'


def test_appointment_serializer_partial_updates_instances_with_invalid_ends_at(appointment):
    # If we only send `start` or `end` in a way that breaks the `end > start` rule,
    # the serializer needs to fail in a safe way
    values = [
        appointment.starts_at,
        appointment.starts_at - timedelta(days=1)
    ]
    for wrong_ends_at in values:
        serializer = AppointmentSerializer(appointment, data={
            'end': wrong_ends_at,
        }, partial=True)

        assert not serializer.is_valid()
        assert str(serializer.errors['period'][0]) == '"end" value must be greater than "start" value.'


@pytest.mark.parametrize('start, end', [
    (tzdatetime(2011, 2, 1), tzdatetime(2010, 12, 25)),
    (tzdatetime(2011, 2, 1), tzdatetime(2011, 2, 1)),
])
def test_appointment_serializer_updates_with_wrong_period_fails(appointment, start, end):
    serializer = AppointmentSerializer(appointment, data={
        'start': start,
        'end': end,
    }, partial=True)

    assert serializer.is_valid() is False
    assert str(serializer.errors['period'][0]) == '"end" value must be greater than "start" value.'


@patch('rest_framework.serializers.ModelSerializer.update')
def test_appointment_serializer_update_unknown_integrity_error_logs_warning(super_update_mock, caplog, appointment):
    super_update_mock.side_effect = IntegrityError('This is an unknown error!')

    # NOTE: for the sake of this test it doesn't
    # matter that we're not changing anything
    # due to the exception to be raised by
    # the mock above
    serializer = AppointmentSerializer(appointment, data={
        'title': 'My appointment',
        'room': appointment.room_id,
        'start': appointment.starts_at,
        'end': appointment.ends_at
    })

    assert serializer.is_valid()

    with pytest.raises(IntegrityError) as excinfo:
        serializer.save()

    assert str(excinfo.value) == 'This is an unknown error!'
