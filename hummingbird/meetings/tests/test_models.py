import pytest

from django.core.exceptions import ValidationError
from django.db.utils import IntegrityError

from hummingbird.meetings.models import Appointment
from .test_utils import tzdatetime

pytestmark = pytest.mark.django_db


@pytest.mark.parametrize('title, type_error', [(None, 'null'), ('', 'blank')])
def test_appointment_title_cant_be_empty(title, type_error, room, period):
    appointment = Appointment(title=title, room=room, period=period)
    with pytest.raises(ValidationError) as excinfo:
        appointment.full_clean()

    message_dict = excinfo.value.message_dict
    assert message_dict.get('title') == [f'This field cannot be {type_error}.']


def test_appointment_must_have_room(period):
    appointment = Appointment(title='Appointment 1', period=period)
    with pytest.raises(ValidationError) as excinfo:
        appointment.full_clean()

    message_dict = excinfo.value.message_dict
    assert message_dict.get('room') == ['This field cannot be null.']


@pytest.mark.parametrize('period', [
    # NOTE: tuples are (start, end)
    (tzdatetime(2018, 2, 1), tzdatetime(2018, 1, 1)),
    (tzdatetime(2018, 1, 1, 12, 22), tzdatetime(2018, 1, 1, 12, 22)),
])
def test_appointment_period_end_must_be_greater_than_start(period, room):
    appointment = Appointment(title='Appointment 1', room=room, period=period)

    with pytest.raises(ValidationError) as excinfo:
        appointment.full_clean()

    message_dict = excinfo.value.message_dict
    assert message_dict.get('period') == ['End value must be greater than the start value']


def test_appointment_periods_cant_conflict(room, period):
    Appointment.objects.create(title='Appointment 1', room=room, period=period)

    with pytest.raises(IntegrityError) as excinfo:
        Appointment.objects.create(title='Appointment 1', room=room, period=period)

    expected_error_message = (
        'conflicting key value violates exclusion constraint '
        '"non_overlapping_appointments"\nDETAIL:  Key (room_id, period)=({room_id}, '
        '["{start}","{end}")) conflicts with '
        'existing key (room_id, period)=({room_id}, ["{start}","{end}")).\n'
    ).format(
        room_id=room.id,
        start=str(period[0]).replace('+00:00', '+00'),
        end=str(period[1]).replace('+00:00', '+00')
    )

    error_message = str(excinfo.value)
    assert error_message == expected_error_message


def test_appointment_periods_cant_overlap(room, period):
    Appointment.objects.create(title='Appointment 1', room=room, period=period)
    appointment_duration = period[1] - period[0]

    # For this test, let's try and make another appointment
    # that starts in the middle of an already created appointment
    other_aptm_start = period[0] + (appointment_duration / 2)
    other_aptm_end = other_aptm_start + appointment_duration
    with pytest.raises(IntegrityError) as excinfo:
        Appointment.objects.create(title='Appointment 1', room=room, period=(other_aptm_start, other_aptm_end))

    expected_error_message = (
        'conflicting key value violates exclusion constraint '
        '"non_overlapping_appointments"\nDETAIL:  Key (room_id, period)=({room_id}, '
        '["{attempted_start}","{attempted_end}")) conflicts with '
        'existing key (room_id, period)=({room_id}, ["{existing_start}","{existing_end}")).\n'
    ).format(
        room_id=room.id,
        existing_start=str(period[0]).replace('+00:00', '+00'),
        existing_end=str(period[1]).replace('+00:00', '+00'),
        attempted_start=str(other_aptm_start).replace('+00:00', '+00'),
        attempted_end=str(other_aptm_end).replace('+00:00', '+00'),
    )

    error_message = str(excinfo.value)
    assert error_message == expected_error_message


def test_appointment_can_start_when_another_ends(room, period):
    Appointment.objects.create(title='Appointment 1', room=room, period=period)

    appointment_duration = period[1] - period[0]
    start, end = period

    # Postgres' exclusion constraint by default considers upper bounds (end timestamps)
    # exclusive, meaning they're not part of the range itself. This makes it possible
    # to have a meeting starting right at the same time another ends
    Appointment.objects.create(title='Appointment 2', room=room, period=(end, end + appointment_duration))
