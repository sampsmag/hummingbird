import pytest
from model_mommy import mommy

import json
from django.urls import reverse
from rest_framework import status

from hummingbird.meetings.models import Appointment
from hummingbird.rooms.models import MeetingRoom
from .test_utils import tzdatetime

pytestmark = pytest.mark.django_db


def test_create_new_appointment(client, room, period):
    response = client.post(reverse('meetings:appointments'), data={
        'title': 'Appointment 1',
        'room': room.id,
        'start': period[0],
        'end': period[1]
    })

    assert response.status_code == status.HTTP_201_CREATED

    appointment_data = json.loads(response.content)
    # we can't safely determine the appointment's id,
    # so we're throwing it out
    del appointment_data['id']

    assert appointment_data == {
        'title': 'Appointment 1',
        'room': room.id,
        'start': period[0].isoformat().replace('+00:00', 'Z'),
        'end': period[1].isoformat().replace('+00:00', 'Z'),
    }


def test_create_new_appointment_empty_title_fails(client, room, period):
    response = client.post(reverse('meetings:appointments'), data={
        'room': room.id,
        'start': period[0],
        'end': period[1]
    })
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert json.loads(response.content) == {
        'title': ['This field is required.']
    }

    response = client.post(reverse('meetings:appointments'), data={
        'title': '',
        'room': room.id,
        'start': period[0],
        'end': period[1]
    })
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert json.loads(response.content) == {
        'title': ['This field may not be blank.']
    }


def test_create_new_appointment_no_room_fails(client, period):
    response = client.post(reverse('meetings:appointments'), data={
        'title': 'Appointment with no room',
        'start': period[0],
        'end': period[1]
    })
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert json.loads(response.content) == {
        'room': ['This field is required.']
    }


def test_create_new_appointment_unknown_room_fails(client, period):
    response = client.post(reverse('meetings:appointments'), data={
        'title': 'Appointment with no room',
        'room': 0,
        'start': period[0],
        'end': period[1]
    })
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert json.loads(response.content) == {
        'room': ['Invalid pk "0" - object does not exist.']
    }


@pytest.mark.parametrize('start, end', [
    (tzdatetime(2011, 2, 1), tzdatetime(2010, 12, 25)),
    (tzdatetime(2011, 2, 1), tzdatetime(2011, 2, 1)),
])
def test_create_new_appointment_invalid_period_fails(client, room, start, end):
    response = client.post(reverse('meetings:appointments'), data={
        'title': 'Appointment with no room',
        'room': room.id,
        'start': start,
        'end': end
    })
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert json.loads(response.content) == {
        'period': ['"end" value must be greater than "start" value.']
    }


def test_create_new_endpoint_with_period_overlap_fails(client, appointment):
    response = client.post(reverse('meetings:appointments'), data={
        'title': 'Appointment A',
        'room': appointment.room_id,
        'start': appointment.starts_at,
        'end': appointment.ends_at
    })
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert json.loads(response.content) == {
        'period': ['Given "start" and "end" values overlap with another appointment in the same room.']
    }


def test_get_list_of_appointments_ordered_by_period(client):
    room_1, room_2, room_3 = mommy.make(MeetingRoom, _quantity=3)

    Appointment.objects.bulk_create([
        Appointment(id=1, title='Meeting of the Rat', room_id=room_1.id,
                    period=(tzdatetime(2018, 1, 2, 3), tzdatetime(2018, 1, 2, 4))),
        Appointment(id=2, title='Meeting of the Dragon', room_id=room_2.id,
                    period=(tzdatetime(2018, 2, 3, 4), tzdatetime(2018, 2, 3, 5))),
        Appointment(id=3, title='Meeting of the Sheep', room_id=room_3.id,
                    period=(tzdatetime(2018, 12, 25, 4), tzdatetime(2018, 12, 25, 20))),
        Appointment(id=4, title='Meeting of the Rooster', room_id=room_2.id,
                    period=(tzdatetime(2018, 2, 3, 7), tzdatetime(2018, 2, 3, 8))),
        Appointment(id=5, title='Meeting of the Horse', room_id=room_1.id,
                    period=(tzdatetime(2018, 1, 2, 5), tzdatetime(2018, 1, 2, 6))),
    ])

    response = client.get(reverse('meetings:appointments'))

    assert response.status_code == status.HTTP_200_OK
    assert json.loads(response.content) == [
        {'id': 1, 'title': 'Meeting of the Rat',
         'room': room_1.id, 'start': '2018-01-02T03:00:00Z', 'end': '2018-01-02T04:00:00Z'},
        {'id': 5, 'title': 'Meeting of the Horse',
         'room': room_1.id, 'start': '2018-01-02T05:00:00Z', 'end': '2018-01-02T06:00:00Z'},
        {'id': 2, 'title': 'Meeting of the Dragon',
         'room': room_2.id, 'start': '2018-02-03T04:00:00Z', 'end': '2018-02-03T05:00:00Z'},
        {'id': 4, 'title': 'Meeting of the Rooster',
         'room': room_2.id, 'start': '2018-02-03T07:00:00Z', 'end': '2018-02-03T08:00:00Z'},
        {'id': 3, 'title': 'Meeting of the Sheep',
         'room': room_3.id, 'start': '2018-12-25T04:00:00Z', 'end': '2018-12-25T20:00:00Z'},
    ]


def test_get_list_of_appointments_filter_by_room(client):
    room_1, room_2, room_3 = mommy.make(MeetingRoom, _quantity=3)

    Appointment.objects.bulk_create([
        Appointment(id=1, title='Meeting of the Rat', room_id=room_1.id,
                    period=(tzdatetime(2018, 1, 2, 3), tzdatetime(2018, 1, 2, 4))),
        Appointment(id=2, title='Meeting of the Dragon', room_id=room_2.id,
                    period=(tzdatetime(2018, 2, 3, 4), tzdatetime(2018, 2, 3, 5))),
        Appointment(id=3, title='Meeting of the Sheep', room_id=room_3.id,
                    period=(tzdatetime(2018, 12, 25, 4), tzdatetime(2018, 12, 25, 20))),
        Appointment(id=4, title='Meeting of the Rooster', room_id=room_2.id,
                    period=(tzdatetime(2018, 2, 3, 7), tzdatetime(2018, 2, 3, 8))),
        Appointment(id=5, title='Meeting of the Horse', room_id=room_1.id,
                    period=(tzdatetime(2018, 1, 2, 5), tzdatetime(2018, 1, 2, 6))),
    ])

    response = client.get(reverse('meetings:appointments') + f'?room={room_1.id}')

    assert response.status_code == status.HTTP_200_OK
    assert json.loads(response.content) == [
        {'id': 1, 'title': 'Meeting of the Rat',
         'room': room_1.id, 'start': '2018-01-02T03:00:00Z', 'end': '2018-01-02T04:00:00Z'},
        {'id': 5, 'title': 'Meeting of the Horse',
         'room': room_1.id, 'start': '2018-01-02T05:00:00Z', 'end': '2018-01-02T06:00:00Z'},
    ]


def test_get_list_of_appointments_filter_by_start_timestamp(client):
    room_1, room_2, room_3 = mommy.make(MeetingRoom, _quantity=3)

    Appointment.objects.bulk_create([
        Appointment(id=1, title='Meeting of the Rat', room_id=room_1.id,
                    period=(tzdatetime(2018, 1, 2, 3), tzdatetime(2018, 1, 2, 4))),
        Appointment(id=2, title='Meeting of the Dragon', room_id=room_2.id,
                    period=(tzdatetime(2018, 2, 3, 4), tzdatetime(2018, 2, 3, 5))),
        Appointment(id=3, title='Meeting of the Sheep', room_id=room_3.id,
                    period=(tzdatetime(2018, 12, 25, 4), tzdatetime(2018, 12, 25, 20))),
        Appointment(id=4, title='Meeting of the Rooster', room_id=room_2.id,
                    period=(tzdatetime(2018, 2, 3, 7), tzdatetime(2018, 2, 3, 8))),
        Appointment(id=5, title='Meeting of the Horse', room_id=room_1.id,
                    period=(tzdatetime(2018, 1, 2, 5), tzdatetime(2018, 1, 2, 6))),
    ])

    response = client.get(reverse('meetings:appointments') + '?start=2018-02-03T07')

    assert response.status_code == status.HTTP_200_OK
    assert json.loads(response.content) == [
        {'id': 4, 'title': 'Meeting of the Rooster',
         'room': room_2.id, 'start': '2018-02-03T07:00:00Z', 'end': '2018-02-03T08:00:00Z'},
        {'id': 3, 'title': 'Meeting of the Sheep',
         'room': room_3.id, 'start': '2018-12-25T04:00:00Z', 'end': '2018-12-25T20:00:00Z'},
    ]


def test_get_list_of_appointments_filter_by_end_timestamp(client):
    room_1, room_2, room_3 = mommy.make(MeetingRoom, _quantity=3)

    Appointment.objects.bulk_create([
        Appointment(id=1, title='Meeting of the Rat', room_id=room_1.id,
                    period=(tzdatetime(2018, 1, 2, 3), tzdatetime(2018, 1, 2, 4))),
        Appointment(id=2, title='Meeting of the Dragon', room_id=room_2.id,
                    period=(tzdatetime(2018, 2, 3, 4), tzdatetime(2018, 2, 3, 5))),
        Appointment(id=3, title='Meeting of the Sheep', room_id=room_3.id,
                    period=(tzdatetime(2018, 12, 25, 4), tzdatetime(2018, 12, 25, 20))),
        Appointment(id=4, title='Meeting of the Rooster', room_id=room_2.id,
                    period=(tzdatetime(2018, 2, 3, 7), tzdatetime(2018, 2, 3, 8))),
        Appointment(id=5, title='Meeting of the Horse', room_id=room_1.id,
                    period=(tzdatetime(2018, 1, 2, 5), tzdatetime(2018, 1, 2, 6))),
    ])

    response = client.get(reverse('meetings:appointments') + '?end=2018-02-03T07')

    assert response.status_code == status.HTTP_200_OK
    assert json.loads(response.content) == [
        {'id': 1, 'title': 'Meeting of the Rat',
         'room': room_1.id, 'start': '2018-01-02T03:00:00Z', 'end': '2018-01-02T04:00:00Z'},
        {'id': 5, 'title': 'Meeting of the Horse',
         'room': room_1.id, 'start': '2018-01-02T05:00:00Z', 'end': '2018-01-02T06:00:00Z'},
        {'id': 2, 'title': 'Meeting of the Dragon',
         'room': room_2.id, 'start': '2018-02-03T04:00:00Z', 'end': '2018-02-03T05:00:00Z'},
    ]


def test_get_list_of_appointments_filter_by_both_start_and_end(client):
    room_1, room_2, room_3 = mommy.make(MeetingRoom, _quantity=3)

    Appointment.objects.bulk_create([
        Appointment(id=1, title='Meeting of the Rat', room_id=room_1.id,
                    period=(tzdatetime(2018, 1, 2, 3), tzdatetime(2018, 1, 2, 4))),
        Appointment(id=2, title='Meeting of the Dragon', room_id=room_2.id,
                    period=(tzdatetime(2018, 2, 3, 4), tzdatetime(2018, 2, 3, 5))),
        Appointment(id=3, title='Meeting of the Sheep', room_id=room_3.id,
                    period=(tzdatetime(2018, 12, 25, 4), tzdatetime(2018, 12, 25, 20))),
        Appointment(id=4, title='Meeting of the Rooster', room_id=room_2.id,
                    period=(tzdatetime(2018, 2, 3, 7), tzdatetime(2018, 2, 3, 8))),
        Appointment(id=5, title='Meeting of the Horse', room_id=room_1.id,
                    period=(tzdatetime(2018, 1, 2, 5), tzdatetime(2018, 1, 2, 6))),
    ])

    response = client.get(reverse('meetings:appointments') + '?start=2018-02-03&&end=2018-02-04')

    assert response.status_code == status.HTTP_200_OK
    assert json.loads(response.content) == [
        {'id': 2, 'title': 'Meeting of the Dragon',
         'room': room_2.id, 'start': '2018-02-03T04:00:00Z', 'end': '2018-02-03T05:00:00Z'},
        {'id': 4, 'title': 'Meeting of the Rooster',
         'room': room_2.id, 'start': '2018-02-03T07:00:00Z', 'end': '2018-02-03T08:00:00Z'},
    ]


def test_get_list_of_appointments_invalid_start_end_timestamps(client):
    response = client.get(reverse('meetings:appointments') + '?start=foo')
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert json.loads(response.content) == {'detail': 'Invalid datetime format: foo'}

    response = client.get(reverse('meetings:appointments') + '?end=bar')
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert json.loads(response.content) == {'detail': 'Invalid datetime format: bar'}


def test_get_appointment_detail(client, appointment):
    response = client.get(reverse('meetings:appointment_detail', args=[appointment.id]))

    assert response.status_code == status.HTTP_200_OK
    assert json.loads(response.content) == {
        'id': appointment.id,
        'room': appointment.room_id,
        'title': appointment.title,
        'start': appointment.starts_at.isoformat().replace('+00:00', 'Z'),
        'end': appointment.ends_at.isoformat().replace('+00:00', 'Z'),
    }


def test_delete_appointment(client, appointment):
    response = client.delete(reverse('meetings:appointment_detail', args=[appointment.id]))

    assert response.status_code == status.HTTP_204_NO_CONTENT

    assert not Appointment.objects.filter(id=appointment.id).exists()


def test_delete_unknown_appointment(client):
    response = client.delete(reverse('meetings:appointment_detail', args=[0]))

    assert response.status_code == status.HTTP_404_NOT_FOUND


def test_put_appointment(client, appointment):
    new_room = mommy.make(MeetingRoom)

    response = client.put(reverse('meetings:appointment_detail', args=[appointment.id]), data={
        'room': new_room.id,
        'title': 'Appointment changed!',
        'start': tzdatetime(2018, 1, 1, 2),
        'end': tzdatetime(2018, 1, 1, 4),
    }, content_type='application/json')

    assert response.status_code == status.HTTP_200_OK

    appointment.refresh_from_db()
    assert appointment.room_id == new_room.id
    assert appointment.title == 'Appointment changed!'
    assert appointment.starts_at == tzdatetime(2018, 1, 1, 2)
    assert appointment.ends_at == tzdatetime(2018, 1, 1, 4)


def test_put_appointment_invalid_period(client, appointment):
    new_room = mommy.make(MeetingRoom)

    response = client.put(reverse('meetings:appointment_detail', args=[appointment.id]), data={
        'room': new_room.id,
        'title': 'Appointment changed!',
        'start': tzdatetime(2018, 1, 1, 4),
        'end': tzdatetime(2018, 1, 1, 2),
    }, content_type='application/json')

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert json.loads(response.content) == {
        'period': ['"end" value must be greater than "start" value.']
    }


def test_patch_appointment(client, appointment):
    response = client.patch(reverse('meetings:appointment_detail', args=[appointment.id]), data={
        'start': tzdatetime(2018, 1, 1, 2),
        'end': tzdatetime(2018, 1, 1, 4),
    }, content_type='application/json')

    assert response.status_code == status.HTTP_200_OK

    appointment.refresh_from_db()
    assert appointment.starts_at == tzdatetime(2018, 1, 1, 2)
    assert appointment.ends_at == tzdatetime(2018, 1, 1, 4)


def test_patch_appointment_invalid_period(client, appointment):
    response = client.patch(reverse('meetings:appointment_detail', args=[appointment.id]), data={
        'start': tzdatetime(2018, 1, 1, 4),
        'end': tzdatetime(2018, 1, 1, 2),
    }, content_type='application/json')

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert json.loads(response.content) == {
        'period': ['"end" value must be greater than "start" value.']
    }


def test_patch_appointment_with_period_overlap(client, appointment):
    # We're not using this directly here, only
    # saving it in the database
    mommy.make(Appointment,
               room_id=appointment.room_id,
               period=(tzdatetime(2018, 2, 3), tzdatetime(2018, 2, 5)))

    response = client.patch(reverse('meetings:appointment_detail', args=[appointment.id]), data={
        'start': tzdatetime(2018, 2, 4),
        'end': tzdatetime(2018, 2, 6),
    }, content_type='application/json')

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert json.loads(response.content) == {
        'period': ['Given "start" and "end" values overlap with another appointment in the same room.']
    }
