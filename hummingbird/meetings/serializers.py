import logging
from django.db.utils import IntegrityError
from rest_framework import serializers

from hummingbird.meetings.models import Appointment, is_integrity_error_overlap_related

_logger = logging.getLogger(__name__)


class AppointmentSerializer(serializers.ModelSerializer):
    start = serializers.DateTimeField()
    end = serializers.DateTimeField()

    class Meta:
        model = Appointment
        exclude = ('period',)

    def to_representation(self, instance):
        return {
            'id': instance.id,
            'title': instance.title,
            'room': instance.room_id,
            'start': instance.starts_at,
            'end': instance.ends_at
        }

    def to_internal_value(self, data):
        validated_data = super().to_internal_value(data)

        start = validated_data.get('start') or getattr(self.instance, 'starts_at', None)
        end = validated_data.get('end') or getattr(self.instance, 'ends_at', None)

        if start and end and not (end > start):
            raise serializers.ValidationError({
                'period': ['"end" value must be greater than "start" value.']
            })

        return validated_data

    def create(self, validated_data):
        self._map_timestamps_to_period_attribute(validated_data)

        try:
            appointment = super().create(validated_data)
        except IntegrityError as exc:
            if not is_integrity_error_overlap_related(exc):
                # We don't want to mask potentially worrying database errors,
                # only the ones we expect should be handled here
                raise

            _logger.info(("Attempt to create appointment for room %s at [%s, %s) "
                          "failed: IntegrityError, overlapping with another appointment"),
                         validated_data['room'].id,
                         validated_data['period'][0],
                         validated_data['period'][1])

            raise serializers.ValidationError({
                'period': ['Given "start" and "end" values overlap with another appointment in the same room.']
            })
        else:
            _logger.info('Appointment %s for room %s created at [%s, %s)',
                         appointment.id,
                         appointment.room_id,
                         appointment.starts_at,
                         appointment.ends_at)

            return appointment

    def update(self, instance, validated_data):
        self._map_timestamps_to_period_attribute(validated_data,
                                                 starts_at=instance.starts_at,
                                                 ends_at=instance.ends_at)

        try:
            appointment = super().update(instance, validated_data)
        except IntegrityError as exc:
            if not is_integrity_error_overlap_related(exc):
                # We don't want to mask potentially worrying database errors,
                # only the ones we expect should be handled here
                raise

            _logger.info("Attempt to update appointment %s for room %s at [%s, %s) failed: IntegrityError",
                         instance.id,
                         validated_data['room'].id if 'room' in validated_data else instance.room_id,
                         validated_data['period'][0],
                         validated_data['period'][1])

            raise serializers.ValidationError({
                'period': ['Given "start" and "end" values overlap with another appointment in the same room.']
            })
        else:
            _logger.info('Appointment %s updated to room %s at [%s, %s)',
                         instance.id,
                         validated_data['room'].id if 'room' in validated_data else instance.room_id,
                         validated_data['period'][0],
                         validated_data['period'][1])

            return appointment

    def _map_timestamps_to_period_attribute(self, validated_data, *, starts_at=None, ends_at=None):
        """
        Maps `start` and `end` fields to a `period` 2-tuple
        to match with the model's implementation
        """
        start = validated_data.pop('start', starts_at)
        end = validated_data.pop('end', ends_at)
        validated_data['period'] = (start, end)
