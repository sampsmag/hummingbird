from django.core.exceptions import ValidationError
from django.contrib.postgres.fields import DateTimeRangeField
from django.db import models

__all__ = [
    'Appointment',
    'is_integrity_error_overlap_related',
]


OVERLAPPING_ERROR = 'conflicting key value violates exclusion constraint "non_overlapping_appointments"'


def is_integrity_error_overlap_related(exc):
    """
    Returns True if given IntegrityError was raised due to the
    exclusion constraint on the Appointment table that ensures
    meetings can't overlap in the same room.
    """
    return str(exc.__cause__).startswith(OVERLAPPING_ERROR)


def validate_end_greater_than_start(value):
    """"
    Validates that period (start, end) has end > start
    """
    start = value.lower
    end = value.upper
    if not end > start:
        raise ValidationError('End value must be greater than the start value')


class Appointment(models.Model):
    """
    Model for a single appointment. The time slot is saved in UTC
    on the attribute `period`. There's an exclusion constraint in
    place to ensure that no overlapping appointments for the same
    room can happen.
    """
    title = models.CharField(max_length=64)
    room = models.ForeignKey('rooms.MeetingRoom', on_delete=models.CASCADE, related_name='appointments')

    # NOTE: Postgres would raise an exception if the upper bound
    # isn't greater or equal to the lower bound. Since saving
    # an appointment with no period (equal lower and upper bounds)
    # doesn't make a lot of sense, we're re-validating so that
    # the upper bound is ALWAYS greater than the lower.
    period = DateTimeRangeField(validators=[validate_end_greater_than_start])

    # NOTE: the properties below have to abstract
    # between self.period being a tuple or a
    # Postgres' DateTimeTZRange object.
    # It's usually the latter when it's retrieved
    # from the database.

    @property
    def starts_at(self):
        """
        Returns the lower bound of self.period,
        i.e. the start date of the appointment
        """
        try:
            return self.period[0]
        except TypeError:
            return self.period.lower

    @property
    def ends_at(self):
        """
        Returns the upper bound of self.period,
        i.e. the end date of the appointment
        """
        try:
            return self.period[1]
        except TypeError:
            return self.period.upper
