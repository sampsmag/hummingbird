from dateutil.parser import parse as parse_date
from django.db.models.functions import Lower, Upper
from rest_framework import generics
from rest_framework import status
from rest_framework.response import Response

from hummingbird.meetings.exceptions import InvalidDateTimeFormatError
from hummingbird.meetings.models import Appointment
from hummingbird.meetings.serializers import AppointmentSerializer

__all__ = [
    'AppointmentsView',
    'AppointmentDetailView',
]


def str_to_datetime(value):
    if value:
        try:
            return parse_date(value)
        except ValueError:
            raise InvalidDateTimeFormatError(value)


class AppointmentsView(generics.ListCreateAPIView):
    serializer_class = AppointmentSerializer

    def get_queryset(self):
        queryset = Appointment.objects.order_by('period')

        room_id = self.request.query_params.get('room', None)
        if room_id:
            queryset = queryset.filter(room_id=room_id)

        start = str_to_datetime(self.request.query_params.get('start', None))
        end = str_to_datetime(self.request.query_params.get('end', None))
        if start and end:
            # NOTE: if both bounds are given, there's a more
            # performant query we can use in comparison
            # to the options used below
            queryset = queryset.filter(period__contained_by=(start, end))
        else:
            if start:
                # NOTE: We're using Django's Lower function here
                # even though the documentation states it's to
                # convert a string to lowercase.
                # This is because PostgreSQL also defines a lower function
                # for range types. In this case, lower(period) means getting
                # the start value
                #
                # https://docs.djangoproject.com/en/1.8/ref/models/database-functions/#lower
                # https://www.postgresql.org/docs/9.3/static/functions-range.html#RANGE-FUNCTIONS-TABLE
                queryset = queryset.annotate(start=Lower('period')).filter(start__gte=start)
            if end:
                # NOTE: Likewise with Django's Upper function:
                # if used on a range type object, it'll return
                # the end value
                #
                # https://docs.djangoproject.com/en/1.8/ref/models/database-functions/#upper
                queryset = queryset.annotate(end=Upper('period')).filter(end__lte=end)

        return queryset

    def get(self, *args, **kwargs):
        try:
            return super().get(*args, **kwargs)
        except InvalidDateTimeFormatError as e:
            return Response({
                'detail': str(e),
            }, status=status.HTTP_400_BAD_REQUEST)


class AppointmentDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Appointment.objects.all()
    serializer_class = AppointmentSerializer
