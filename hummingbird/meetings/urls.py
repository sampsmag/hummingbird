from django.urls import path

from hummingbird.meetings.views import AppointmentsView, AppointmentDetailView


app_name = 'meetings'
urlpatterns = [
    path('', AppointmentsView.as_view(), name='appointments'),
    path('<int:pk>', AppointmentDetailView.as_view(), name='appointment_detail'),
]
