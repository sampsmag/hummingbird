

class InvalidDateTimeFormatError(Exception):
    """
    To be thrown when the user submits
    a datetime string that can't be parsed.
    """

    def __init__(self, value):
        super().__init__(f'Invalid datetime format: {value}')
