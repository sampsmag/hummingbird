from setuptools import setup


setup(
    name='hummingbird',
    version='0.0.1',
    description='Meeting room reservation manager',
    license='',
    packages=['hummingbird'],
    install_requires=[
        'Django>=2.1',
        'djangorestframework>=3.8',
        'python-decouple',
        'psycopg2',
        'dj-database-url',
        'python-dateutil',
    ]
)
