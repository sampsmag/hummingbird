help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

all: build run  ## Builds and runs the project

build:  ## Builds and install runtime and development dependencies
	docker-compose build hummingbird
	docker-compose run --rm hummingbird dockerfiles/hummingbird/install-deps.sh

run:  ## Runs all services in non-detachable mode
	docker-compose up

flushdb:  ## Removes ALL DATA from the database, including schemas!
	docker-compose run --rm hummingbird dockerfiles/hummingbird/flush-database.sh

test:  ## Runs the test suite with tox in a disposable container
	docker-compose run --rm hummingbird pipenv run tox

.PHONY: help build flushdb run all test
