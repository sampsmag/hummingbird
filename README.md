# Hummingbird - the meeting room scheduler!

Because naming things is hard, I chose _hummingbird_. ~~At least
they're cute~~

This project uses Python 3.7, Django 2.1 and PostgreSQL as a
database. Because we're relying on a specific pg data type, PostgreSQL
as a dependency is mandatory.


## How to run this project

There's a `Makefile` with shortcuts to most commands needed to
administer this project. It's self documented, so you can type `make
help` to take a look.

Still, here's how to get this project up and running. You'll need
`docker` and `docker-compose`. The version file used in
`docker-compose.yml` is `3.2` so if you see an error related to that,
consider upgrading your `docker-compose`.

The project depends on a few environment variables, all of which are
documented in `example.env`, so you can just copy it to your local
environment:

    # .env is git-ignored!
    $ cp example.env .env
    
You can now open your `.env` and take a look, every environment
variable there is documented. Unless you diverge from the default
setup behavior, I recommend only changing `HUMMINGBIRD_DATAD_DIR` to a
directory of your liking.

For a development environment, that's all there is! Now you need to
download and/or build the whole docker container environment. You can
do so by running:

    $ make build
    
This will a) build the container hummingbird itself will run off of,
and b) install its Python dependencies inside.

After that step is done, you can start the server by running:

    $ make run
    
The init script bound to the command above will run any pending
migrations before bringing the server up, so by now you should be good
to go. You can also run `build + run` in a single step by running:

    $ make all
    
## Running tests

Run `make test` to run the test suite. It'll call `tox`, which will,
in turn, run `py.test` and then `flake8`.

Note that the tests are run in a freshly instantiated container, which
is then removed when tests are done.

## API documentation

### /rooms/

#### GET /rooms/

Returns a list of rooms ordered by their names.

#### POST /rooms/

Creates a new room. The only argument it accepts is `name`, a
string. It has to be unique, otherwise the action will fail.

#### GET /rooms/<pk>

Returns a given room.

#### (PUT|PATCH) /rooms/<pk>

Updates a given room. Since a meeting room only has `name` as an
attribute, `PUT` and `PATCH` are equivalent in its calls.

#### DELETE /rooms/<pk>

Deletes a room.

### /appointments/

#### GET /appointments/

Returns all appointments ordered by their scheduled period, i.e.  the
start of the meetings.

Data can be filtered by using any combination of the following query
params:

- `room`, integer, returns only appointments for the given room
- `start`, ISO 8601 string, returns only appointments that start from
  the given timestamp onward
- `end`, ISO 8601 string, returns only appointments that end up to the
  given timestamp
  
Naturally these filters can be combined. If `start` and `end` are used
together, this endpoint will only return appointments that begin and
end within the given bounds.

#### POST /appointments/

Creates a new appointment. Its arguments are:

- `room`, integer, mandatory, the id of the meeting room
- `title`, string, can be anything you choose
- `start`, ISO 8601 string of the start of the appointment
- `end`, ISO 8601 string of the end of the appointment

All values are required. The following validations are performed:

- `end` MUST be greater than `start`. No appointments can have zero
  scheduled time (when `start = end`)
- there MUST BE NO overlaps for the same room. If the given `start`
  and `end` overlap with another saved appointment for the same room,
  this call will fail.
  
The overlap validation above _does not apply_ when an appointment
starts at the exact same time another ends. The reason for that is
that we're saving the upper bound (the `end` value) as exclusive,
meaning that strictly speaking it's not part of the range.

#### GET /appointments/<pk>

Returns a given appointment.

#### PUT /appointments/<pk>

Replaces the appointment's attributes. All four attributes listed
in `POST /appointments/` MUST be given again. The same validations
apply.

#### PATCH /appointments/<pk>

Allows for partial updates of the given appointment. All validations
listed in `POST /appointments/` apply. If only `start` or `end` are
given, but not the other, hummingbird will assume the current value
for validation.

In other words (values for illustration only):

- Appointment `X` has `start=12` and `end=23`
- `PATCH /appointments/X start=14`
- hummingbird will validate with `start=14` and `end=23`

#### DELETE /appointments/<pk>

Deletes a given appointment.

## Assumptions and trade-offs 

By deciding to use the `tstzrange` data type for the period and
relying on an exclusion constraint to keep consistency for the
appointments, we now have a side-effect: we can no longer safely
ensure there are no overlaps before attempting the INSERT/UPDATE. In
other words, we can no longer rely on Django model's and DRF's
validators alone.

As a side effect from above, we're checking for `IntegrityError`
exceptions related to overlaps and then re-wrapping them as
`ValidationError` at the serializer level, since these database
exceptions in particular are expected and can be handled well.  In an
unlikely case of an unknown `IntegrityError` being raised, though,
it'll not be handled and will return `500 SERVER ERROR`.  Defensive
programming aside, I consider that better behavior: it prevents the
using umbrella catch-all handlers, at the very least.

Still, unknown errors are unlikely to happen under normal
circumstances.

I've developed this by testing with UTC timestamps only. The behavior
when non-UTC timestamps are used is _untested_. For an application of
this caliber, I believe it's a good idea to sanitize input timestamps
somehow, but I've decided to not do now to not over-engineer
hummingbird, since there are some different ways to deal with it.
